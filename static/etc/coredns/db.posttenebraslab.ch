$ORIGIN posttenebraslab.ch.
$TTL 300	; 5 minutes

;;;; authority ;;;;
@	IN SOA	ns1.posttenebraslab.ch. comite.posttenebraslab.ch. (
		499        ; serial
		300        ; refresh (5 minutes)
		300        ; retry (5 minutes)
		604800     ; expire (1 week)
		300        ; minimum (5 minutes)
	)
@	NS	ns1.posttenebraslab.ch.
@	TXT	"google-site-verification=myQ4Q1LPKker7TNWAiT7Rmj6qsQQdocYn_Yo3XQvHdQ"
smtp	TXT	"google-site-verification=MPYh5ojJWYdwYgC5fowWTgnn17WjPiDS1nBtsFPvScU"

;;;; e-mail ;;;;
@	MX	10 smtp.posttenebraslab.ch.
@	TXT	"v=spf1 ip4:82.197.186.97 ip4:85.195.210.243 -all"
_dmarc	TXT	"v=DMARC1; p=none; pct=10; ruf=mailto:comite-owner@posttenebraslab.ch!1m; fo=0:1:s:d;"
default._domainkey	TXT	"v=DKIM1; h=sha256; k=rsa; " "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2Rhqf6BelCM0S2dcf8qKtwqKspdBZALtDuTsGD9i3/6/FAadKVERQcgl+dIgmHbZV3nuRplT+h4NmPIFj8auF6/hR6fy2umy5lxIevpp5gEHjnTa0zLsPEmVh+KJt5vHm+R+6g6GqnhMW0CKTkeScWUXdt9l0hYezyKQ8q5OWFzMktcZaptmBiXbGaHwem8CSz+mJJcQ20BMh7" "d/12W+wtOQSt+w09l8rNWxoqWBVrYB/dAVjmBGnmZB3ExsREtk+2JbVuAMEGy3F+gXw5LKh6fgN/RwNkulQaATDMT7MIKkOcCb1UPR2GXnESJyhhir/dPNBflyrz5TL6kK3VbzYQIDAQAB"
default._domainkey.smtp	TXT	"v=DKIM1; h=sha256; k=rsa; " "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2Rhqf6BelCM0S2dcf8qKtwqKspdBZALtDuTsGD9i3/6/FAadKVERQcgl+dIgmHbZV3nuRplT+h4NmPIFj8auF6/hR6fy2umy5lxIevpp5gEHjnTa0zLsPEmVh+KJt5vHm+R+6g6GqnhMW0CKTkeScWUXdt9l0hYezyKQ8q5OWFzMktcZaptmBiXbGaHwem8CSz+mJJcQ20BMh7" "d/12W+wtOQSt+w09l8rNWxoqWBVrYB/dAVjmBGnmZB3ExsREtk+2JbVuAMEGy3F+gXw5LKh6fgN/RwNkulQaATDMT7MIKkOcCb1UPR2GXnESJyhhir/dPNBflyrz5TL6kK3VbzYQIDAQAB"

;;;; kerberos ;;;;
_kerberos-master._tcp	SRV	0 100 88 freeipa.lan.posttenebraslab.ch.
_kerberos-master._udp	SRV	0 100 88 freeipa.lan.posttenebraslab.ch.
_kerberos._tcp	SRV	0 100 88 freeipa.lan.posttenebraslab.ch.
_kerberos._udp	SRV	0 100 88 freeipa.lan.posttenebraslab.ch.
_kerberos	TXT	"POSTTENEBRASLAB.CH"
_kerberos	URI	0 100 "krb5srv:m:tcp:freeipa.lan.posttenebraslab.ch."
_kerberos	URI	0 100 "krb5srv:m:udp:freeipa.lan.posttenebraslab.ch."
_kpasswd._tcp	SRV	0 100 464 freeipa.lan.posttenebraslab.ch.
_kpasswd._udp	SRV	0 100 464 freeipa.lan.posttenebraslab.ch.
_kpasswd	URI	0 100 "krb5srv:m:tcp:freeipa.lan.posttenebraslab.ch."
_kpasswd	URI	0 100 "krb5srv:m:udp:freeipa.lan.posttenebraslab.ch."
_ldap._tcp	SRV	0 100 389 freeipa.lan.posttenebraslab.ch.
ipa-ca	A	10.42.0.10

;;;; ip ;;;;
@	A	82.197.186.97
ns1	A	82.197.186.97
smtp	A	82.197.186.97	; matches reverse DNS
intranet	A	85.195.210.243	; matches reverse DNS

;;;; others ;;;;
cloud	CNAME	intranet
matrix	CNAME	@
www	CNAME	@
