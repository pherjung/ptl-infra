$ORIGIN .
$TTL 600	; 10 minutes
ptlino.com		IN SOA	ns1.posttenebraslab.ch. ns2.posttenebraslab.ch. (
				116        ; serial
				3600       ; refresh (1 hour)
				3600       ; retry (1 hour)
				604800     ; expire (1 week)
				3600       ; minimum (1 hour)
				)
			NS	ns1.posttenebraslab.ch.
			NS	ns2.posttenebraslab.ch.
			A	85.195.210.243
$ORIGIN ptlino.com.
www			CNAME	ptlino.com.
